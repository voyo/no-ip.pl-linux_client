/*                                                                                                                                                                                  
 *                                                                                                                                                                                  
 * Copyright 2002 - 2021, Wojtek Sawasciuk <voyo@no-ip.pl>                                                                                                                     
 *
 * Client program for dynamicaly updating IP addreses in DNS.                                                                                                                                                                                  
 * See http://www.no-ip.pl for more information about                                                                                                                         
 * this program and purpose. 
 *                                                                                                                                                                                  
 * This program is free software, distributed under the terms of                                                                                                                    
 * the GNU General Public License Version 2. See the LICENSE file                                                                                                                   
 * at the top of the source tree.                                                                                                                                                   
 *
 */                                                                                                                                                                                 
      

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdarg.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>


#include "common.h"

#define MIN_UPDATE 5
#define BUFFER_SIZE  1000
#define MAXBUFF 100

#define VERSION "1.12"

void err_sys (char buff[100]) { perror(buff); exit(1); }

int print_help()
{
printf("Program przetwarza argumenty: \n\t-u login\n\t-p pass\n\t-d domena\n");
printf("\t-V wyświetla aktualną wersje programu\n\t-b N  uruchamia się w tle, i aktualizuje dns co N minut (min 5)\n");
return(0);
}

int print_version()
{
printf("Klient dynamicznego dns'u - www.no-ip.pl\nWersja %s\n",VERSION);
return(0);
}



int main(int argc, char *argv[])
{
    int sd, clilen, servlen;
    struct sockaddr_in  servaddr;
    struct hostent  *hptr;
    struct in_addr  **pptr;

    char buf[MAXMESG];
    struct message w;
    struct return_message ret_msg;
    int sockfd;
    int n;
    char* args = "u:p:d:a:vhVb:";
    int ret;
    int verbose = 0;
    int background = 0;
    int update;
    char server_address_ip[255];
    
    int pid;
    int help = 0;    
    char *login,*pass,*domena,*ip;
    

login=(void *)malloc(MAXBUFF);
pass=(char *)malloc(MAXBUFF);
domena=(char *)malloc(MAXBUFF);
ip=(char *)malloc(MAXBUFF);

strcpy(ip,"0.0.0.0");    



if (argc==1) { print_help(); exit(1); };


 while((ret = getopt(argc, argv, args)) != -1)
   {
       switch (ret)
           {
         case 'u' : strncpy(login,optarg,MAXBUFF-1); break;
         case 'p' : strncpy(pass,optarg,MAXBUFF-1); break;
         case 'd' : strncpy(domena,optarg,MAXBUFF-1); break;
         case 'a' : strncpy(ip,optarg,MAXBUFF-1); break;
         case 'v' : verbose = 1; break;
         case 'V' : print_version(); exit(1);
         case 'b' : background = 1; update=strtol(optarg,NULL,16); break;
         case 'h' :
         case '?' : /* brak paramatrow */
         help = 1;
       break;
  }
}

/* sprawdzenie czy sa parametry, jak brakuje to exit */
if (help==1) printf("brak argumentów\n");

if (background && update<MIN_UPDATE) {printf("za mała wartość parametru update\n"); exit(1);}

if (background)  {

pid=fork();

if ( pid == 0)    { }
 else if (pid < 0) { 
    perror("Fork failure \n");
       exit(-4);
   }  else  { if(verbose) printf("Klient działa w tle, update co %d minut\n",update); return 0; }
}
       



    bzero(&servaddr, sizeof(servaddr));
    bzero(&w,sizeof(w));
    
    
    
    if((hptr = (struct hostent *)gethostbyname((char *)SERVER_ADDRESS))==NULL) {
    	printf("gethostbyname error for host: %s\n",SERVER_ADDRESS);
    	/*  hsterror(h_errno));  */
 	exit(1);
 	}
 	
    pptr = (struct in_addr **) hptr->h_addr_list;
    
memcpy(&servaddr.sin_addr,(struct in_addr **) *hptr->h_addr_list,sizeof(struct in_addr));  
    
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(SERVER_PORT);
/*    inet_pton(AF_INET,server_address_ip,&servaddr.sin_addr); */

    if((sd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        err_sys("client: can't open datagram socket");


            w.version=MESSAGE_VERSION;	
            w.id_msg=1;

/* printf("l %s,%s,%s\n",login,pass,domena); */

strcpy(w.login,login);
strcpy(w.pass,pass);
strcpy(w.domena,domena);
strcpy(w.ip,ip);

do {		

if (verbose) printf("wysy�am: ver_msg=%d,login=%s,pass=%s,ip=%s,domena=%s,id_msg=%d\n",w.version,w.login,w.pass,w.ip,w.domena,w.id_msg); 

	    sendto(sd,&w,sizeof(w),0,  (struct sockaddr *)&servaddr,sizeof(servaddr));
	    
	  /* wylaczone narazie, nie czeka na kody bledy, moglo by byc, ale trzeba dorobic timeout - nawypadek padu serwera */  
/*              n=recvfrom(sd,&ret_msg,MAXMESG,0,NULL,NULL);
 if (verbose)  printf("dosta�em spowrotem: %d,%d,%d\n",ret_msg.version,ret_msg.error_code,ret_msg.id_msg);  */
 if (background) sleep(update*60);

} while (background);


free(login);
free(pass);
free(domena);
free(ip);
close(sd);

}
