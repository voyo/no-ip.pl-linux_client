struct message {
int version;
char login[32];
char pass[32];
char ip[16];
char domena[64];
int id_msg;
};

#define MAXMESG sizeof(struct message)


struct return_message {
int version;
int error_code;
int id_msg;
char ip[16];
};

#define MAXRETURNMESG sizeof(struct return_message)


#define MESSAGE_VERSION 2
#define RETURN_MESSAGE_VERSION 2


#define ERR_OK 0
#define ERR_AUTHORIZATION_FAILED 1
#define ERR_NO_UPDATE 2
#define ERR_BAD_VERSION 3
#define ERR_ACCESS_DENIED 4
#define ERR_NOT_ENOUGH_RIGHTS 5

#define ERR_UNKNOWN 255


#define SERVER_PORT 7777
#define SERVER_ADDRESS  "update.no-ip.pl"



#define USER named


/* noip user */
uid_t uid=1006;

